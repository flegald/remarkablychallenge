
# Remarkably Interview

### Env

Install python packages:
    `pip install -r requirements.txt`
    
### Execution

#### Command line
Args:

`--kpi_list`: A comma separated string of desired KPIs.
	
`--start`: Beginning date for metric range.
	
	
`--stop`: End date for metric range.
	

Example:
`python remarkably.py --kpi_list occupancy,light,co2 --start "2/2/1999" --stop "2/3/2020"`

#### Python File
To input arguments directly into python file, add you arguments to the KPICalculator init, starting on line 106.

Example:
```
results = KPICalculator(  
	  kpi_list=["occupancy", "light" ,"co2"],  
	  start="2/2/1999",  
	  stop="2/3/2020",  
	  raw_data=raw_data  
).run_calculator()
```

### Notes

The API linked in https://gist.github.com/jason-c-child/13336d507f19e71cd6eae38cfa85368e,
does not always work. It will periodically return an empty dataset or fail entirely.

I have added the results in a file, `raw_data.txt` that the program will pull from in the case the API fails.
 
