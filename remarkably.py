import requests
import argparse
from dateutil.parser import parse
import json
from statistics import mean, median, mode

API_URL = "http://lameapi-env.ptqft8mdpd.us-east-2.elasticbeanstalk.com/data"


def parse_command_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--start", dest="start", required=False, help="Start date of time period")
    parser.add_argument("--stop", dest="stop", required=False, help="Stop date of the time period")
    parser.add_argument("--kpi_list", dest="kpi_list", required=False, help="Comma delimited list of kpis")
    args = parser.parse_args()
    return {"start": args.start, "stop": args.stop, "kpi_list": args.kpi_list}


def fetch_data():
    try:
        fetched_data = requests.get(API_URL).json()["data"]
        if not len(fetched_data):
            raise Exception("No Data Present")
        return fetched_data
    except Exception as e:
        print(f"[Error] Can not retrieve data from source, using stored file: {e}")
        with open("raw_data.txt", "r") as ifile:
            fetched_data = ifile.read()
        return fetched_data


class KPICalculator:

    def __init__(self, kpi_list, start, stop, raw_data):
        self.kpi_list = kpi_list
        self.time_frame = {"start": parse(start), "stop": parse(stop)}
        self.raw_data = raw_data
        self.results = dict()

    @staticmethod
    def format_entry(entry):
        entry = entry.replace(" ", ",").split(",")
        return {
            "date": parse(entry[0]),
            "temperature": entry[1],
            "humidity": entry[2],
            "light": entry[3],
            "co2": entry[4],
            "humidity_ratio": entry[5],
            "occupancy": entry[6]
        }

    def init_results(self):
        for kpi in self.kpi_list:
            self.results[kpi.lower()] = {
                "last_value": None,
                "first_value": None,
                "lowest": None,
                "highest": None,
                "mode": None,
                "average": None,
                "median": None,
                "all_entries": []
            }

    def check_date_between(self, date):
        frame = self.time_frame
        return frame["start"] <= date <= frame["stop"]

    def transform_raw_data(self):
        self.raw_data = self.raw_data.replace("\\n", "\n").splitlines()
        for idx, entry in enumerate(self.raw_data):
            if idx == 0: continue
            formatted_entry = self.format_entry(entry)
            if not self.check_date_between(formatted_entry["date"]):
                continue
            for kpi in self.results.keys():
                metric = float(formatted_entry[kpi])
                self.results[kpi]["all_entries"].append(metric)

    def calculate_results(self):
        for kpi in self.results.keys():
            result = self.results[kpi]
            all_entries = result["all_entries"]
            result["average"] = round(mean(all_entries), 3)
            result["median"] = round(median(all_entries), 3)
            result["mode"] = round(mode(all_entries), 3)
            result["lowest"] = min(all_entries)
            result["highest"] = max(all_entries)
            result["first_value"] = all_entries[0]
            result["last_value"] = all_entries[-1]
            del result["all_entries"]

    def run_calculator(self):
        self.init_results()
        self.transform_raw_data()
        self.calculate_results()
        return self.results


if __name__ == "__main__":
    command_line_args = parse_command_args()
    raw_data = fetch_data()

    results = KPICalculator(
        kpi_list=command_line_args["kpi_list"].split(","),
        start=command_line_args["start"],
        stop=command_line_args["stop"],
        raw_data=raw_data
    ).run_calculator()

    print(json.dumps(results, indent=4))

